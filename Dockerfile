#Pull image from dockerhub image repository
From tomcat:8-jre8 

# Maintainer
MAINTAINER "arvind.aws.cloud@gmail.com" 

# copy war file on to container 
COPY ./webapp.war /usr/local/tomcat/webapps